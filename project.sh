#!/bin/bash

if [ "$1" = "-d" ] || [ "$1" = "--debug" ]; then
        export DEBUG='true'
        shift 1
fi

### ajout dossier bin/ a PATH
SCRIPT_PATH="$(dirname "$(readlink -f "$0")")"
( cd "$SCRIPT_PATH" && make )
# shellcheck source=libtoolbox.sh
. "$SCRIPT_PATH"/libtoolbox.sh

### Set default parameters
action=$1
repo=$2
export repoDir
repoDir="$(echo "$repo" | sed -E 's;.*\/(.*)\..*;\1;')"
export projName
projName="$(echo "$repoDir" | sed -E 's;.*_(.*);\1;')"
export userDir
userDir=$PWD
cd "$userDir" || { echo "Error cd in $userDir" && exit; }
export INIT_PWD
INIT_PWD=$(pwd)

check_sudo

if [ "$action" != 'clone' ] && [ "$action" != 'remove' ] && [ "$action" != 'fix-permissions' ]
then
	echo "You need to prompt for action clone, remove or fix-permissions --Lower-case only"
	exit 1;
fi

###################
## Clone action ##
###################

if [ "$action" = 'clone' ]; then
  if [ ! -z "$repoDir" ]; then
    echo "The project name we found is : $projName"
    echo "Do you want to use this name for the project ? (y/n)"
    read -r a
    if [ "$a" = "${a#[Yy]}" ]; then
      echo "Please type the project name you want to use -> "
      read -r projName
    fi
  fi

  echo "Please check that you are in your working directory, are you ? (y/n)"
  read -r answer
  if [ "$answer" != "${answer#[Yy]}" ] ;then
    ## On demande la techno du project
    get_techno techno
    export techno
    get_public_path "$techno" public_path
    export public_path

    ### check if directory exists or not
    if  [ -d "$userDir/$repoDir" ]; then
      echo "Repo already exist, sorry my friend"
      exit 1
    else
      ### clonning
      su --login --command "cd \"$userDir\" && git clone -b dev $repo" "$(logname)"

      sudo mv "$repoDir" "$projName".git

      #Permissions rules
      export PROJECT_NAME
      PROJECT_NAME=$projName
      export PROJECT_HOSTNAME
      PROJECT_HOSTNAME=$projName.local
      # shellcheck disable=SC2031
      export SERVER_USER
      SERVER_USER=$(logname)
      export PROJECT_TECH
      PROJECT_TECH=$techno
      export PROJECT_PATH
      PROJECT_PATH=$userDir/$projName.git
      server_initialize_project

      root=$userDir/$projName.git/$public_path
      create_vh "$root" "$projName"

      echo "Enter your mysql username [default => admin]"
      read -r user
      echo "Enter your password [default => yeswedev]"
      read -r pass
      if [ -z "$user" ]; then
        user="admin"
      fi
      if [ -z "$pass" ]; then
        pass="yeswedev"
      fi
      retrieve_database "$user" "$pass" "$projName"

      case "$PROJECT_TECH" in
        ('wordpress'|'laravel')
          update_env_file "$user" "$pass" "$projName"
        ;;
      esac

      case "$PROJECT_TECH" in
        ('wordpress')
          cd "$PROJECT_PATH" || exit
          curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
          php8.0 wp-cli.phar search-replace "https://$projName.dev.yeswedev.fr" "http://$projName.local" --skip-columns=guid  --allow-root
          theme_name=$projName-theme
          if [ "$DEBUG" = 'true' ]; then
            su --login --command "cd $PROJECT_PATH/web/app/themes/$theme_name && composer install -n" "$SERVER_USER"
          else
            su --login --command "cd $PROJECT_PATH/web/app/themes/$theme_name && composer install -n" "$SERVER_USER" >/dev/null 2>&1
          fi

          if [ "$DEBUG" = 'true' ]; then
            su --login --command "cd $PROJECT_PATH/web/app/themes/$theme_name && yarn && yarn build" "$SERVER_USER"
          else
            su --login --command "cd $PROJECT_PATH/web/app/themes/$theme_name && yarn && yarn build" "$SERVER_USER" >/dev/null 2>&1
          fi
          rm wp-cli.phar
          chown --recursive "$SERVER_USER".www-data "$PROJECT_PATH/web/app/uploads"
          retrieve_wordpress_upload "$projName"
        ;;
      esac

      ### show the finish message
      echo "Complete! You now have a new Virtual Host Your new host is: http://$projName.local"
      if [ "$DEBUG" = 'true' ]; then
        printenv > allVar.txt
        echo "All var printed in allVar.txt"
      fi
      exit 0
    fi
  fi
fi

###################
## Remove action ##
###################

if [ "$action" = 'remove' ]; then
  shift 1
	if [ "$1" != "" ]; then
		projFolder=$1

    case $projFolder in
      *.git*)
          echo "This is a project";;
      *)
          echo "This is not a project -> project folders end with '.git'"
          exit 1;;
    esac

		proj="$(echo "$projFolder" | sed -E 's;(.+)\..+;\1;')"
		if [ "$DEBUG" = 'true' ]; then
			echo "Debugging activated !"
			echo "Deleting project folder"
		fi
    if [ -d "$projFolder" ]; then
		  rm -r "$projFolder"
    else
      echo "Ce dossier n'existe pas"
      exit 1
    fi
		if [ "$DEBUG" = 'true' ]; then
			echo "Project folder deleted"
		fi

		if [ "$DEBUG" = 'true' ]; then
			echo "Deleting conf files but before we check if they exists"
		fi
		if [ -f "/etc/nginx/sites-available/$proj.local.conf" ] && [ -f "/etc/nginx/sites-enabled/$proj.local.conf" ]; then
			rm /etc/nginx/sites-available/"$proj".local.conf
			rm /etc/nginx/sites-enabled/"$proj".local.conf
		else
			echo "The files $proj.local.conf do not exist"
		fi
		service nginx reload
		if [ "$DEBUG" = 'true' ]; then
			echo "conf files stuff done"
		fi

		if [ "$DEBUG" = 'true' ]; then
			echo "Dropping the database"
		fi
    echo "Please type your mysql user [default => admin] : "
		read -r user
		if [ -z "$user" ]; then user="root"; fi
    echo "Please type your mysql password [default => yeswedev] : "
		read -r pass
		if [ -z "$pass" ]; then pass="root"; fi
		if database_exists "$proj" "$user" "$pass"; then
			mysql -u "$user" -p"$pass" -e "drop database \`$proj\`"
		else
			echo "No database found"
		fi
		if [ "$DEBUG" = 'true' ]; then
			echo "Database stuff done"
		fi

		if [ "$DEBUG" = 'true' ]; then
			echo "Saving then editing /etc/hosts"
		fi
		sed -i.bak "/.* ${proj}.local$/d" /etc/hosts
		if [ "$DEBUG" = 'true' ]; then
			cat /etc/hosts
		fi
		echo "Host removed from /etc/hosts"
		if [ "$DEBUG" = 'true' ]; then
			echo "/etc/hosts edited"
		fi
	else
		echo "No project specified"
	fi
fi


############################
## fix permissions action ##
############################

if [ "$action" = 'fix-permissions' ]; then
  shift 1
  cd "$SCRIPT_PATH" || { echo "Error cd in $SCRIPT_PATH" && exit; }
  actions/fix-permissions.sh "$@"
  [ "$DEBUG" = 'true' ] && echo "Done"
fi
