#!/bin/sh
set -e

SCRIPT_PATH="$(dirname "$(readlink --canonicalize "$0")")"

( cd "$SCRIPT_PATH" && make )
# shellcheck source=libtoolbox.sh
. "$SCRIPT_PATH/libtoolbox.sh"
check_files "$SCRIPT_PATH/toolbox.conf"
# shellcheck source=toolbox.conf.example
. "$SCRIPT_PATH/toolbox.conf"

# Multiple hosts for servers are not implemented yet
ask_for_server_host
echo "Utilisation de $SERVER_HOST"

# Create server from user-provided name
create_server

# Initial server setup
if [ "$SERVER_INITIAL_SETUP" = 'true' ]; then
	echo 'En attente de la création du serveur…'
	sleep 2m
	server_get_ip
	check_variables 'SERVER_IP'
	ssh-keyscan -v -t rsa "$SERVER_IP" >> ~/.ssh/known_hosts
	server_send_toolbox
	server_initial_setup
fi

exit 0
