# Boîte à outils Yes We Dev

## Installation

Depuis la racine d’un projet git :
```
git submodule add https://framagit.org/yeswedev/ywd_toolbox.git ywd_toolbox
git submodule update --init --recursive ywd_toolbox
```

## Mise-à-jour

Depuis la racine du projet :
```
git submodule update --recursive --remote ywd_toolbox
```

## Configuration

Depuis la racine du projet :
```
cp ywd_toolbox/toolbox.conf.example ywd_toolbox/toolbox.conf
```
Puis éditez le fichier ywd_toolbox/toolbox.conf pour y renseigner les informations relatives à votre projet.
```bash
DEBUG='false' # mode verbeux

## Project

PROJECT_NAME='myproject'         # nom du projet (format machine)
PROJECT_TECH='ipot'              # technologie utilisée par le projet (ex : 'laravel', 'wordpress', etc.)
PROJECT_HOSTNAME='example.com'   # nom de domaine du projet

## Servers                               
                                         
# DigitalOcean                           
DIGITALOCEAN_TOKEN='abcd12345'      # token d’accès à l’API DigitalOcean
DIGITALOCEAN_REGION='ams3'          # région du serveur
DIGITALOCEAN_SIZE='s-1vcpu-1gb'     # type du serveur
DIGITALOCEAN_IMAGE='21669205'       # id de l’image à utiliser comme base
DIGITALOCEAN_SSH_KEYS='12345,67890' # liste des id des clés SSH autorisées à accéder au serveur

# Actions
SERVER_INITIAL_SETUP='true' # configuration initiale automatique du serveur
```

## Utilisation

### Création de serveur

Depuis la racine du projet :
```
./ywd_toolbox/create-server.sh
```
