all: libtoolbox.sh

libtoolbox.sh: lib/*
	@if [ -f "libtoolbox.sh" ]; then\
		rm -f libtoolbox.sh;\
		echo "libtoolbox.sh supprimé";\
	fi
	@cat lib/* > libtoolbox.sh
	@echo "libtoolbox.sh créé"

install:
	@sudo ln -sf "$(shell pwd)/project.sh" "/usr/local/bin/project"
	@echo "Lien symbolique créé dans /usr/local/bin"
	@sudo cp setup/project_autocompletion /etc/bash_completion.d/project
	@echo "Autocomplete ajouté"

clean:
	@if [ -f "libtoolbox.sh" ]; then\
		rm -f libtoolbox.sh;\
		echo "libtoolbox.sh supprimé";\
	else\
		echo "libtoolbox.sh n'existe pas";\
	fi

uninstall:
	@if [ -f "/usr/local/bin/project" ]; then\
		sudo rm -f /usr/local/bin/project;\
		echo "project sym-link supprimé";\
	else\
		echo "project sym-link n'existe pas";\
	fi
	@if [ -f "/etc/bash_completion.d/project" ]; then\
		sudo rm -f /etc/bash_completion.d/project;\
		echo "Auto-completion supprimé";\
	else\
		echo "Le fichier d'auto-completion n'existe pas";\
	fi
