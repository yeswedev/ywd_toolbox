#!/bin/sh
set -o errexit

SCRIPT_PATH="$(dirname "$(readlink -f "$0")")/.."
( cd "$SCRIPT_PATH" && make )
. $SCRIPT_PATH/libtoolbox.sh

if [ -n "$INIT_PWD" ]; then
  cd "$INIT_PWD" # The directory where project.sh has been called
fi               # else this script has been direcly called so INIT_PWD is not defined

if [ "$#" -ne "1" ]; then
  echo "You need 1 argument - a folder"
  exit 1;
else
  if [ -d "$1" ]; then
    folder="$(readlink -f "$1")"
    case ${folder##/*/} in
      *.git*)
          [ "$DEBUG" = 'true' ] && echo "This is a project folder";;
      *)
          echo "This is not a project -> project folders end with '.git'"
          exit 1;;
    esac
    PATH_PROJECT="$(echo $folder | sed -E 's;/$;;')"
    [ "$DEBUG" = 'true' ] && echo "PATH_PROJECT='$PATH_PROJECT'"
  else
    echo "Argument is not a folder"
    exit 1
  fi
fi

echo "Please make sure to run this script only on a local machine, not on development or production environment" && sleep 10

USER="$(logname)"
GROUP='www-data'

cd "$PATH_PROJECT"
[ "$DEBUG" = 'true' ] && printf "Current directory -> "; pwd

[ "$DEBUG" = 'true' ] && echo "PROJECT_TECH='$PROJECT_TECH'"
if [ -z "$PROJECT_TECH" ]; then
  echo "What technology this project is using ?"
  echo "1 - Laravel"
  echo "2 - WordPress (using Bedrock)"
  echo "3 - Else"
  read answer
  case "$answer" in
    ('1')
      PROJECT_TECH="laravel"
      ;;
    ('2')
      PROJECT_TECH="wordpress"
      ;;
    (*)
      echo "This technology is not supported yet"
      exit 0
      ;;
  esac
fi

[ "$DEBUG" = 'true' ] && echo "PROJECT_TECH='$PROJECT_TECH'"

get_environment

if [ -n "$SERVER_ENVIRONMENT" ] && [ "$SERVER_ENVIRONMENT" = "local" ]; then
  echo "Now running permissions fix"
  case "$PROJECT_TECH" in
    ('laravel')
      if [ "$(id --user)" = 0 ]; then
              chgrp --recursive "$GROUP" "$PATH_PROJECT/bootstrap" "$PATH_PROJECT/storage"
              chmod --recursive ug+rwX "$PATH_PROJECT/bootstrap" "$PATH_PROJECT/storage"
      else
              sudo chgrp --recursive "$GROUP" "$PATH_PROJECT/bootstrap" "$PATH_PROJECT/storage"
              sudo chmod --recursive ug+rwX "$PATH_PROJECT/bootstrap" "$PATH_PROJECT/storage"
      fi
      ;;
    ('wordpress')
      if [ "$(id --user)" = 0 ]; then
              chown --recursive "$USER"."$GROUP" ./*
              chmod --recursive g+rw ./*
      else
              sudo chown --recursive "$USER"."$GROUP" ./*
              sudo chmod --recursive g+rw ./*
      fi
      ;;
    (*)
      echo "This technology is not supported yet"
      exit 0
      ;;
  esac
  echo "Permissions fixed !"
else
  if [ -n "$SERVER_ENVIRONMENT" ]; then
    echo "SERVER_ENVIRONMENT='$SERVER_ENVIRONMENT'"
    echo "Not on local environment, won't start"
    echo "Please make sure to run this script only on a local machine, not on development or production environment"
    exit 1;
  else
    [ "$DEBUG" = 'true' ] && echo "SERVER_ENVIRONMENT is empty"
  fi
fi

exit 0
