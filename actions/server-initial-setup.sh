#!/bin/sh
set -e

SCRIPT_PATH="$(dirname "$(readlink --canonicalize "$0")")/.."

# shellcheck source=libtoolbox.sh
. "$SCRIPT_PATH/libtoolbox.sh"
check_files "$SCRIPT_PATH/toolbox.conf"
# shellcheck source=toolbox.conf.example
. "$SCRIPT_PATH/toolbox.conf"

if [ "$(id --user)" != 0 ]; then
	echo "Erreur: Ce script doit être exécuté par le compte root."
	exit 1
fi

# Add a swap file
fallocate --length 1G /var/swap
chmod 600 /var/swap
mkswap /var/swap
swapon /var/swap
cat >> /etc/fstab << 'EOF'
# 1GB swap file
/var/swap none swap sw 0 0
EOF

# Setup APT sources
cat > /etc/apt/sources.list << EOF
deb http://mirrors.digitalocean.com/ubuntu/ bionic main restricted universe multiverse
deb http://mirrors.digitalocean.com/ubuntu/ bionic-updates main restricted universe multiverse
deb http://mirrors.digitalocean.com/ubuntu/ bionic-backports main restricted universe multiverse
deb http://security.ubuntu.com/ubuntu bionic-security main restricted universe multiverse
EOF
echo 'Mise à jour du système…'
export DEBIAN_FRONTEND=noninteractive
if [ "$DEBUG" = 'true' ]; then
	sleep 5s
	apt-get update
	sleep 5s
	apt-get upgrade --assume-yes
else
	sleep 5s
	apt-get update --quiet=2 >/dev/null 2>&1
	sleep 5s
	apt-get upgrade --assume-yes --quiet=2 >/dev/null 2>&1
fi

# Create user account
echo 'Création du compte utilisateur…'
USER_NAME='yeswedev'
useradd --create-home --groups www-data --shell /bin/bash $USER_NAME

# Setup user account SSH access
# shellcheck disable=SC1112
echo 'Configuration de l’accès SSH…'
ssh_dir=/home/$USER_NAME/.ssh
mkdir --parents "$ssh_dir"
cp /root/.ssh/authorized_keys "$ssh_dir"
chown --recursive $USER_NAME. "$ssh_dir"
chmod 700 "$ssh_dir"
chmod 600 "$ssh_dir/authorized_keys"

# Set up timezone
export DEBIAN_FRONTEND=noninteractive
export DEBCONF_DB_OVERRIDE="File{$SCRIPT_PATH/debconf/config.dat}"
# shellcheck disable=SC1112
echo 'Configuration de l’environnement…'
echo 'Europe/Paris' > /etc/timezone
ln --force --symbolic ../usr/share/zoneinfo/Europe/Paris /etc/localtime
dpkg-reconfigure --frontend=noninteractive tzdata >/dev/null 2>&1
apt-get install --assume-yes --no-install-recommends --quiet=2 ntp sntp >/dev/null 2>&1

# Set up locales
locale-gen --purge en_US.UTF-8 fr_FR.UTF-8 >/dev/null 2>&1
update-locale --reset LANG='fr_FR.UTF-8' LANGUAGE='fr_FR:fr'

# Install base packages for a typical Web application
apt-get install --assume-yes --no-install-recommends --quiet=2 make >/dev/null 2>&1
echo 'Installation de MariaDB…'
apt-get install --assume-yes --no-install-recommends --quiet=2 mariadb-server mariadb-client >/dev/null 2>&1
echo 'Installation de nginx…'
apt-get install --assume-yes --no-install-recommends --quiet=2 nginx apache2-utils >/dev/null 2>&1
echo 'Installation de git…'
apt-get install --assume-yes --no-install-recommends --quiet=2 git >/dev/null 2>&1
echo 'Installation de certbot…'
apt-get install --assume-yes --no-install-recommends --quiet=2 certbot python3-certbot-nginx >/dev/null 2>&1

# Install PHP 7.3 + PHPMyAdmin
PHP_VERSION='7.3'
echo "Installation de PHP ${PHP_VERSION}…"
add-apt-repository ppa:ondrej/php 2>&1
apt-get install --assume-yes --no-install-recommends --quiet=2 php${PHP_VERSION} php${PHP_VERSION}-curl php${PHP_VERSION}-fpm php${PHP_VERSION}-gd php${PHP_VERSION}-mbstring php${PHP_VERSION}-mysql php${PHP_VERSION}-xml php${PHP_VERSION}-zip libapache2-mod-php${PHP_VERSION}*- >/dev/null 2>&1
echo 'Installation de PHPMyAdmin…'
apt-get install --assume-yes --no-install-recommends --quiet=2 phpmyadmin >/dev/null 2>&1
echo 'Installation de Composer…'
apt-get install --assume-yes --no-install-recommends --quiet=2 composer >/dev/null 2>&1

# Install Node.js from nodesource third-party repository
echo 'Installation de Node.js et npm…'
curl -sSL https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add -
cat > /etc/apt/sources.list.d/nodejs.list << 'EOF'
deb https://deb.nodesource.com/node_10.x bionic main
EOF
apt-get update --quiet=2 >/dev/null 2>&1
apt-get install --assume-yes --no-install-recommends --quiet=2 nodejs >/dev/null 2>&1

# Create database and associated user
echo 'Création de la base de données…'
DB_HOST='localhost'
DB_NAME="$(printf '%s' "$PROJECT_NAME" | sed 's/-/_/g')"
DB_USER="$(printf '%s' "$PROJECT_NAME" | sed 's/-/_/g')"
DB_PASS="$(password_generate 32)"
mysql -u root << EOF
create database $DB_NAME;
grant all privileges on $DB_NAME.* to '$DB_USER'@'localhost' identified by '$DB_PASS';
flush privileges;
EOF

# Set higher upload limit for nginx and PHP
max_upload_size='128'
echo "Définition de la limite d’upload (${max_upload_size}M)"
php_conf="/etc/php/${PHP_VERSION}/fpm/php.ini"
pattern="s/^\\(upload_max_filesize\\) = .*/\\1 = ${max_upload_size}M/"
pattern="$pattern;s/^\\(post_max_size\\) = .*/\\1 = ${max_upload_size}M/"
sed --in-place "$pattern" "$php_conf"
service php${PHP_VERSION}-fpm restart
nginx_conf='/etc/nginx/conf.d/upload.conf'
mkdir --parents "$(dirname "$nginx_conf")"
cat > "$nginx_conf" << EOF
client_max_body_size ${max_upload_size}m;
EOF
service nginx reload

# Improve nginx gzip configuration
nginx_conf='/etc/nginx/conf.d/gzip.conf'
sed --in-place 's/^[^#]\s*gzip/#&/' /etc/nginx/nginx.conf
mkdir --parents "$(dirname "$nginx_conf")"
cat > "$nginx_conf" << EOF
gzip on;
gzip_vary on;
gzip_types application/javascript image/x-icon text/css text/plain image/svg+xml;
EOF
service nginx reload

# Set default headers for HTTP requests
nginx_conf='/etc/nginx/conf.d/gzip.conf'
mkdir --parents "$(dirname "$nginx_conf")"
cat > "$nginx_conf" << EOF
add_header X-Content-Type-Options "nosniff";
add_header X-Frame-Options "SAMEORIGIN";
add_header X-XSS-Protection "1; mode=block";
EOF
service nginx reload

# Configure Web server
echo 'Configuration du serveur web…'
HTTP_ROOT=/srv/http/$PROJECT_HOSTNAME
HTTP_USER=$USER_NAME
HTTP_PASS="$(password_generate 16)"
HTTP_AUTHFILE=/srv/http/htpasswd
HTTP_LOGS=/var/log/nginx/$PROJECT_HOSTNAME
HTTP_LOGS_PHPMYADMIN=/var/log/nginx/phpmyadmin
mkdir --parents "$HTTP_ROOT"
cat > "$HTTP_ROOT/index.html" << EOF
<h1>It works!</h1>
<p>Project: $PROJECT_NAME</p>
EOF
cat > "$HTTP_ROOT/phpinfo.php" << EOF
<?php phpinfo(); ?>
EOF
mkdir --parents "$(dirname $HTTP_AUTHFILE)"
htpasswd -i -c "$HTTP_AUTHFILE" "$HTTP_USER" >/dev/null 2>&1 << EOF
$HTTP_PASS
EOF
for directory in "$HTTP_LOGS" "$HTTP_LOGS_PHPMYADMIN"; do
	mkdir --parents "$directory"
	chown www-data.adm "$directory"
	chmod ug+rwxs "$directory"
done
cat > "/etc/nginx/sites-available/$PROJECT_HOSTNAME" << EOF
server {
	server_name $PROJECT_HOSTNAME;

	listen 80;
	listen [::]:80;

#	return 301 https://\$host\$request_uri;
#}

#server {
#	server_name $PROJECT_HOSTNAME;

#	listen 443 ssl http2;
#	listen [::]:443 ssl http2;

#	ssl_certificate     /etc/letsencrypt/live/$PROJECT_HOSTNAME/fullchain.pem;
#	ssl_certificate_key /etc/letsencrypt/live/$PROJECT_HOSTNAME/privkey.pem;

	root $HTTP_ROOT;

	access_log $HTTP_LOGS/access.log;
	error_log  $HTTP_LOGS/error.log;

	index index.php index.html index.htm;

	auth_basic           "Zone restreinte";
	auth_basic_user_file $HTTP_AUTHFILE;

	location / {
		try_files \$uri \$uri/ /index.php?\$query_string;
	}

	error_page 404 /index.php;

	location = /favicon.ico {
		access_log off;
		log_not_found off;
	}
	location = /robots.txt {
		access_log off;
		log_not_found off;
	}

	location ~ \\.php$ {
		include snippets/fastcgi-php.conf;
		fastcgi_pass unix:/run/php/php${PHP_VERSION}-fpm.sock;
	}

	location ~ /\\.(?!well-known).* {
		deny all;
	}
}
EOF
ln --symbolic "../sites-available/$PROJECT_HOSTNAME" /etc/nginx/sites-enabled
cat > "/etc/nginx/sites-available/phpmyadmin.$PROJECT_HOSTNAME" << EOF
server {
	server_name phpmyadmin.$PROJECT_HOSTNAME;

	listen 80;
	listen [::]:80;

#	return 301 https://\$host\$request_uri;
#}

#server {
#	server_name phpmyadmin.$PROJECT_HOSTNAME;

#	listen 443 ssl http2;
#	listen [::]:443 ssl http2;

#	ssl_certificate     /etc/letsencrypt/live/$PROJECT_HOSTNAME/fullchain.pem;
#	ssl_certificate_key /etc/letsencrypt/live/$PROJECT_HOSTNAME/privkey.pem;

	root /usr/share/phpmyadmin;

	access_log $HTTP_LOGS_PHPMYADMIN/access.log;
	error_log  $HTTP_LOGS_PHPMYADMIN/error.log;

	index index.php index.html index.htm;

	auth_basic           "Zone restreinte";
	auth_basic_user_file $HTTP_AUTHFILE;

	location / {
		try_files \$uri \$uri/ /index.php?\$query_string;
	}

	error_page 404 /index.php;

	location = /favicon.ico {
		access_log off;
		log_not_found off;
	}
	location = /robots.txt {
		access_log off;
		log_not_found off;
	}

	location ~ \\.php$ {
		include snippets/fastcgi-php.conf;
		fastcgi_pass unix:/run/php/php${PHP_VERSION}-fpm.sock;
	}

	location ~ /\\.(?!well-known).* {
		deny all;
	}
}
EOF
ln --symbolic "../sites-available/phpmyadmin.$PROJECT_HOSTNAME" /etc/nginx/sites-enabled
service nginx reload

# Enable Web server logs rotation
cat > /etc/logrotate.d/nginx << 'EOF'
/var/log/nginx/*.log /var/log/nginx/*/*.log {
	daily
	missingok
	rotate 14
	compress
	delaycompress
	notifempty
	create 0640 www-data adm
	sharedscripts
	prerotate
		if [ -d /etc/logrotate.d/httpd-prerotate ]; then \
			run-parts /etc/logrotate.d/httpd-prerotate; \
		fi \
	endscript
	postrotate
		invoke-rc.d nginx rotate >/dev/null 2>&1
	endscript
}
EOF

SERVER_IP="$(curl --silent --ipv4 ifconfig.pro)"

# Install and set-up vim
echo 'Installation et configuration de vim…'
apt-get install --assume-yes --no-install-recommends --quiet=2 vim-nox vim-addon-manager >/dev/null 2>&1
vim-addon-manager install -w nginx
sed --in-place 's/^"set background=.*$/set background=dark/' /etc/vim/vimrc

echo ''
echo '##########'
echo ''
echo 'Configuration du serveur terminée.'
echo ''
echo 'Ajouter lʼhôte au fichier /etc/hosts local'
echo "echo '$SERVER_IP $PROJECT_HOSTNAME phpmyadmin.$PROJECT_HOSTNAME' | sudo tee --append /etc/hosts"
echo ''
echo 'Accès HTTP'
echo "URL : http://$PROJECT_HOSTNAME/"
echo "Utilisateur : $HTTP_USER"
echo "Mot de passe : $HTTP_PASS"
echo ''
echo 'Infos PHP'
echo "http://$PROJECT_HOSTNAME/phpinfo.php"
echo ''
echo 'Infos dʼaccès à la base de données'
echo "Hôte : $DB_HOST"
echo "Nom : $DB_NAME"
echo "Utilisateur : $DB_USER"
echo "Mot de passe : $DB_PASS"
echo ''
echo 'PHPMyAdmin'
echo "http://phpmyadmin.$PROJECT_HOSTNAME/?pma_username=$DB_USER&pma_password=$(printf '%s' "$DB_PASS" | sed 's/+/%2B/g')"
echo ''
echo 'Accès via SSH'
echo "ssh $USER_NAME@$PROJECT_HOSTNAME"
echo ''
echo '##########'
echo ''

exit 0
