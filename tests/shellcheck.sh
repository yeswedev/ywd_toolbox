#!/bin/sh
set -o errexit

ROOT_DIR="$(readlink --canonicalize "$(dirname "$0")/..")"

(
	cd "$ROOT_DIR"
	make
	for shell in sh bash dash ksh; do
		printf 'Testing code syntax in %s mode…\n' "$shell"
		shellcheck --shell="$shell" tests/*.sh
		shellcheck --external-sources --shell="$shell" lib/libcommon.sh lib/libserver.sh lib/libprestashop.sh
		shellcheck --external-sources --shell="$shell" create-server.sh deploy.sh
		shellcheck --external-sources --shell="$shell" actions/server-initial-setup.sh
	done
	printf 'Testing Bash-specific code syntax…\n'
	shellcheck --external-sources --shell=bash lib/libclone.sh
	shellcheck --external-sources --shell=bash project.sh
)

exit 0
