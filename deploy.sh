#!/bin/sh
set -e

REPO_URL='https://framagit.org/yeswedev/ywd_deploy'
printf 'Project deployment has been moved to a dedicated project, see %s for more informations.\n' "$REPO_URL"

exit 0
