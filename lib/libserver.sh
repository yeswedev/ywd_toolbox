# Common functions
# shellcheck disable=SC2039

ask_for_server_host() {
	unset SERVER_HOST
	unset SERVER_ID
	# shellcheck disable=SC1112
	echo 'Sélectionnez l’hébergeur à utiliser dans la liste suivante :'
	echo '1 — DigitalOcean'
	echo '2 — Infomaniak (pas encore géré)'
	while [ -z "$SERVER_ID" ]; do
		printf 'Entrez le numéro correspondant à votre choix : '
		read -r SERVER_ID
	done
	case "$SERVER_ID" in
		('1') SERVER_HOST='DigitalOcean' ;;
		('2') SERVER_HOST='Infomaniak' ;;
		(*)
			echo "Erreur : $SERVER_ID n’est pas un choix valide."
			exit 1
			;;
	esac
	set_server_host "$SERVER_HOST"
}
set_server_host() {
	SERVER_HOST="$1"
	case "$SERVER_HOST" in
		('DigitalOcean') digitalocean_set_server_host ;;
		(*)
			echo "Erreur : l’hôte $1 n’est pas géré."
			exit 1
			;;
	esac
	export SERVER_HOST
}
create_server() {
	check_variables 'SERVER_HOST'
	case "$SERVER_HOST" in
		('DigitalOcean') digitalocean_create_server ;;
		(*)
			echo "Erreur : l’hôte $SERVER_HOST n’est pas géré."
			exit 1
			;;
	esac
}
server_send_toolbox() {
	check_dependencies 'rsync'
	server_get_ip
	check_variables 'SERVER_IP'

	# shellcheck disable=SC2086
	rsync --links --recursive "$SCRIPT_PATH/" root@$SERVER_IP:ywd_toolbox/

	return 0
}
server_get_ip() {
	check_variables 'SERVER_HOST'
	case "$SERVER_HOST" in
		('DigitalOcean')
			digitalocean_get_server_ip
			SERVER_IP="$DROPLET_IP"
			export SERVER_IP
			;;
		(*)
			echo "Erreur : l’hôte $SERVER_HOST n’est pas géré."
			exit 1
			;;
	esac

	return 0
}
server_initial_setup() {
	check_dependencies 'ssh'
	server_get_ip
	check_variables 'SERVER_IP'

	# shellcheck disable=SC2086
	ssh root@$SERVER_IP ywd_toolbox/actions/server-initial-setup.sh

	return 0
}
server_initialize_project() {
	check_variables 'PROJECT_TECH'
	case "$PROJECT_TECH" in
		('laravel')
			server_initialize_laravel_project
			;;
		('wordpress')
			server_initialize_wordpress_project
			;;
		('prestashop'*)
			server_initialize_prestashop_project
			;;
		('drupal')
			server_initialize_drupal_project
			;;
		(*) ;;
	esac
}
server_initialize_laravel_project() {
	printf 'Initialisation du projet Laravel…\n'
	check_dependencies 'php' 'composer' 'npm'
	check_variables 'PROJECT_NAME' 'SERVER_USER'
	if [ -z "$PROJECT_PATH" ]; then
		PROJECT_PATH="/home/$SERVER_USER/$PROJECT_NAME.git"
	fi
	if ! [ -x "$PROJECT_PATH/artisan" ]; then
		chmod 775 "$PROJECT_PATH"/artisan
	fi
	# shellcheck source=config/auth.json.example
	cp "$SCRIPT_PATH/config/nova/auth.json" "$PROJECT_PATH/auth.json"
	if [ "$DEBUG" = 'true' ]; then
		su --login --command "cd $PROJECT_PATH && cp .env.example .env && composer install && ./artisan key:generate && npm install && npm run dev" "$SERVER_USER"
	else
		su --login --command "cd $PROJECT_PATH && cp .env.example .env && composer install && ./artisan key:generate && npm install && npm run dev" "$SERVER_USER" >/dev/null 2>&1
	fi
	chmod 664 "$PROJECT_PATH"/artisan
	chgrp --recursive 'www-data' "$PROJECT_PATH/bootstrap" "$PROJECT_PATH/storage"
	chmod --recursive ug+rwX "$PROJECT_PATH/bootstrap" "$PROJECT_PATH/storage"
	printf '\033[1;32mOK\033[0m\n'
}
server_initialize_wordpress_project() {
	printf 'Initialisation du projet WordPress + Bedrock…\n'
	check_dependencies 'php' 'composer' 'npm' 'yarn'
	check_variables 'PROJECT_NAME' 'SERVER_USER' 'PROJECT_HOSTNAME'
	if [ -z "$PROJECT_PATH" ]; then
		PROJECT_PATH="/home/$SERVER_USER/$PROJECT_NAME.git"
	fi
	if [ "$DEBUG" = 'true' ]; then
		su --login --command "cd $PROJECT_PATH && cp .env.example .env" "$SERVER_USER"
	else
		su --login --command "cd $PROJECT_PATH && cp .env.example .env" "$SERVER_USER" >/dev/null 2>&1
	fi
	file="$PROJECT_PATH/.env"
	check_files "$file"
	pattern="s#^\\(WP_HOME\\)=.*#\\1=https://$PROJECT_HOSTNAME#"
	for variable in 'AUTH_KEY' 'SECURE_AUTH_KEY' 'LOGGED_IN_KEY' 'NONCE_KEY' 'AUTH_SALT' 'SECURE_AUTH_SALT' 'LOGGED_IN_SALT' 'NONCE_SALT'; do
		pattern="$pattern;#^\\($variable\\)=.*#\\1='$(password_generate 64)'"
	done
	check_files "$SCRIPT_PATH/config/wordpress.conf"
	# shellcheck source=config/wordpress.conf.example
	. "$SCRIPT_PATH/config/wordpress.conf"
	export WP_ACF_PRO_KEY
	export WP_ROCKET_KEY
	export WP_ROCKET_EMAIL
	export WP_PLUGIN_GF_KEY
	printf 'Utilisation de %s\n' "$SCRIPT_PATH/config/wordpress.conf"
	file="$PROJECT_PATH/.env"
	check_files "$file"
	sed --in-place "s/^ACF_PRO_KEY=.*/ACF_PRO_KEY=$WP_ACF_PRO_KEY/" "$file"
	sed --in-place "s/^WP_ROCKET_KEY=.*/WP_ROCKET_KEY=$WP_ROCKET_KEY/" "$file"
	sed --in-place "s/^WP_ROCKET_EMAIL=.*/WP_ROCKET_EMAIL=$WP_ROCKET_EMAIL/" "$file"
	sed --in-place "s/^WP_PLUGIN_GF_KEY=.*/WP_PLUGIN_GF_KEY=$WP_PLUGIN_GF_KEY/" "$file"
	sed --in-place "$pattern" "$file"
	if [ "$DEBUG" = 'true' ]; then
		su --login --command "cd $PROJECT_PATH && composer install" "$SERVER_USER"
	else
		su --login --command "cd $PROJECT_PATH && composer install" "$SERVER_USER" >/dev/null 2>&1
	fi

	project_dir_name="${PROJECT_PATH##/*/}"
	theme_name="${project_dir_name%%.*}-theme"
	if [ "$DEBUG" = 'true' ]; then
		su --login --command "cd $PROJECT_PATH/web/app/themes/$theme_name && composer install -n" "$SERVER_USER"
	else
		su --login --command "cd $PROJECT_PATH/web/app/themes/$theme_name && composer install -n" "$SERVER_USER" >/dev/null 2>&1
	fi

	chgrp --recursive "www-data" "$PROJECT_PATH"
	chmod --recursive g+rw "$PROJECT_PATH"

	printf '\033[1;32mOK\033[0m\n'
}
server_initialize_drupal_project() {
	printf 'Initialisation du projet Drupal…\n'
	check_dependencies 'composer'
	check_variables 'PROJECT_NAME' 'SERVER_USER'
	if [ -z "$PROJECT_PATH" ]; then
		PROJECT_PATH="/home/$SERVER_USER/$PROJECT_NAME.git"
	fi
	if [ "$DEBUG" = 'true' ]; then
		su --login --command "cd $PROJECT_PATH && composer install" "$SERVER_USER"
	else
		su --login --command "cd $PROJECT_PATH && composer install" "$SERVER_USER" >/dev/null 2>&1
	fi
	directory="$PROJECT_PATH/config"
	mkdir "$directory"
	# shellcheck disable=SC2086
	chown $SERVER_USER.www-data "$directory"
	printf '\033[1;32mOK\033[0m\n'
}

# DigitalOcean
digitalocean_set_server_host() {
	SERVER_API_URL_PREFIX='https://api.digitalocean.com/v2'
	export SERVER_API_URL_PREFIX
	if [ "$DEBUG" = 'true' ]; then
		printf 'SERVER_API_URL_PREFIX="%s"\n' "$SERVER_API_URL_PREFIX"
	fi
}
digitalocean_get_common_curl_options_get() {
	check_variables 'DIGITALOCEAN_TOKEN'
	CURL_OPTIONS="$CURL_OPTIONS --request GET"
	# shellcheck disable=SC2089
	CURL_OPTIONS="$CURL_OPTIONS --header 'Authorization: Bearer $DIGITALOCEAN_TOKEN'"
	# shellcheck disable=SC2090
	export CURL_OPTIONS
}
digitalocean_get_common_curl_options_post() {
	check_variables 'DIGITALOCEAN_TOKEN'
	CURL_OPTIONS="$CURL_OPTIONS --request POST"
	CURL_OPTIONS="$CURL_OPTIONS --header 'Content-Type: application/json'"
	CURL_OPTIONS="$CURL_OPTIONS --header 'Authorization: Bearer $DIGITALOCEAN_TOKEN'"
	# shellcheck disable=SC2090
	export CURL_OPTIONS
}
digitalocean_create_server() {
	# shellcheck disable=SC2039
	local curl_data
	# shellcheck disable=SC2039
	local curl_options
	# shellcheck disable=SC2039
	local curl_url

	check_variables 'PROJECT_HOSTNAME' 'DIGITALOCEAN_REGION' 'DIGITALOCEAN_SIZE' 'DIGITALOCEAN_IMAGE' 'DIGITALOCEAN_SSH_KEYS' 'SERVER_API_URL_PREFIX'
	digitalocean_get_common_curl_options_post
	check_variables 'CURL_OPTIONS'

	curl_data='{'
	curl_data="$curl_data \"name\": \"$PROJECT_HOSTNAME\","
	curl_data="$curl_data \"region\": \"$DIGITALOCEAN_REGION\","
	curl_data="$curl_data \"size\": \"$DIGITALOCEAN_SIZE\","
	curl_data="$curl_data \"image\": \"$DIGITALOCEAN_IMAGE\","
	curl_data="$curl_data \"ssh_keys\": [ $DIGITALOCEAN_SSH_KEYS ],"
	curl_data="$curl_data \"ipv6\": true,"
	curl_data="$curl_data \"monitoring\": true"
	curl_data="$curl_data }"
	curl_options="$CURL_OPTIONS --data '$curl_data'"
	curl_url="$SERVER_API_URL_PREFIX/droplets"

	send_api_call "$curl_options" "$curl_url"
	unset CURL_OPTIONS

	# shellcheck disable=SC2001
	DROPLET_ID="$(echo "$API_FEEDBACK" | sed 's/{"droplet":{"id":\([^,]*\),.*}/\1/')"
	if [ "$DEBUG" = 'true' ]; then
		echo "Id du droplet : $DROPLET_ID"
	fi
	export DROPLET_ID

	return 0
}
digitalocean_get_server_ip() {
	# shellcheck disable=SC2039
	local curl_options
	# shellcheck disable=SC2039
	local curl_url

	check_variables 'DROPLET_ID'
	digitalocean_get_common_curl_options_get
	check_variables 'CURL_OPTIONS'

	curl_options="$CURL_OPTIONS"
	curl_url="$SERVER_API_URL_PREFIX/droplets/$DROPLET_ID"

	send_api_call "$curl_options" "$curl_url"
	unset CURL_OPTIONS

	# shellcheck disable=SC2001
	DROPLET_IP="$(echo "$API_FEEDBACK" | sed 's/{"droplet":.*"networks":{"v4":\[{"ip_address":"\([^"]*\)",.*}/\1/')"
	if [ "$DEBUG" = 'true' ]; then
		echo "IP du droplet : $DROPLET_IP"
	fi
	export DROPLET_IP

	return 0
}
