# PrestaShop-specific functions

server_initialize_prestashop_project() {
	printf 'Initialisation du projet PrestaShop…\n'
	check_dependencies 'composer'
	check_variables 'PROJECT_NAME' 'PROJECT_TECH'
	# shellcheck disable=SC2031
	if [ -z "$PROJECT_PATH" ]; then
		PROJECT_PATH="/home/$SERVER_USER/$PROJECT_NAME.git"
	fi
	for directory in 'config' 'download' 'img' 'mails' 'modules' 'themes' 'translations' 'upload' 'var'; do
		chgrp --recursive 'www-data' "$PROJECT_PATH/$directory"
		chmod --recursive ug+rwX "$PROJECT_PATH/$directory"
	done
	case "$PROJECT_TECH" in
		('prestashop1.6')
			for directory in 'cache' 'log'; do
				chgrp --recursive 'www-data' "$PROJECT_PATH/$directory"
				chmod --recursive ug+rwX "$PROJECT_PATH/$directory"
			done
		;;
		(*)
			for directory in 'app/config' 'app/Resources/translations'; do
				chgrp 'www-data' "$PROJECT_PATH/$directory"
				chmod ug+rwX "$PROJECT_PATH/$directory"
			done
		;;
	esac
	printf '\033[1;32mOK\033[0m\n'
}

