# Common helper functions
# shellcheck disable=SC2039

check_dependencies() {
	for dependency in "$@"; do
		if ! command -v "$dependency" 1>/dev/null 2>&1; then
			echo "Erreur : la commande $dependency est introuvable."
			exit 1
		fi
	done
}
check_files() {
	for file in "$@"; do
		if [ ! -f "$file" ]; then
			echo "Erreur : le fichier $file n’existe pas."
			exit 1
		fi
	done
}
check_variables() {
	for name in "$@"; do
		# shellcheck disable=SC2086
		if [ -z "$(eval printf -- '%b' \"\$$name\")" ]; then
			echo "Erreur : la variable $name n’est pas définie."
			exit 1
		fi
	done
}
password_generate() {
	check_dependencies 'base64'
	# shellcheck disable=SC2039
	local bytes_count
	# shellcheck disable=SC2039
	local password
	bytes_count="$1"
	password="$(dd if=/dev/urandom of=/dev/stdout bs=64 count=1 2>/dev/null | base64 - | head --lines=1 | head --bytes="$bytes_count")"
	printf '%s' "$password"
	return 0
}
send_api_call() {
	curl_options="$1"
	curl_url="$2"
	check_variables 'curl_options' 'curl_url'
	check_dependencies 'curl'
	if [ "$DEBUG" = 'true' ]; then
		echo curl "$curl_options" \""$curl_url"\"
	fi
	# shellcheck disable=SC2046,SC2086,SC2116
	API_FEEDBACK="$(eval $(echo curl $curl_options "$curl_url") 2>/dev/null)"
	export API_FEEDBACK
	if [ "$DEBUG" = 'true' ]; then
		echo "$API_FEEDBACK"
	fi
	return 0
}

