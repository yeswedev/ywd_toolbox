# shellcheck disable=SC2039

check_sudo() {
	if [ "$(whoami)" != 'root' ]; then
        	echo "You should run this as sudo"
					exit 1;
	fi
}

check_db_exists(){
	if [ -d /var/lib/mysql/"$1" ]; then
		return 1
	fi
}

get_techno() {
	printf "Which technology this project is using ? [1 => laravel ; 2 => wordpress ; 3 => drupal ; 4 => prestashop ; 5 => custom]\\n"
	read -r answer
	case $answer in
				1)
        	techno="laravel"
                ;;
        2)
                techno="wordpress"
                ;;
        3)
                techno="drupal"
                ;;
        4)
                techno="prestashop"
                ;;
        5)
                techno="autre"
                ;;
        *)
                echo "\"$answer\" n'est pas une valeur acceptée."
		exit 1
                ;;
	esac

	eval "$1"=$techno
}

get_public_path() {
        case $1 in
        "laravel")
                pointer="public"
                ;;
        "wordpress")
                pointer="web"
                ;;
        "drupal")
                pointer=""
                ;;
        "prestashop")
                pointer=""
                ;;
        "custom")
                pointer=""
                ;;
        *)
                pointer=""
                ;;
        esac

        eval "$2"=$pointer
}

create_vh() {
	if [ "$#" != '2' ]; then
		echo "Wrong arg numbers"
		exit 1
	else
		root=$1
		projName=$2
	fi
  ### on Récupère la version de php
  phpvers="$(php -v | sed -e '/^[^PHP]/d' -e 's/PHP \([0-9]\.[0-9]\).*/php\1/')"

	### create virtual host rules file
	if ! sudo sh -c "echo 'server {
	listen 80;
	listen [::]:80;

	root $root;
	index index.php index.html;
	server_name $projName.local www.$projName.local;

	access_log /var/log/nginx/$projName.access.log;
	error_log /var/log/nginx/$projName.error.log;

	client_max_body_size 100M;
	client_body_timeout 120s;
	expires off;

	location / {
	try_files \$uri \$uri/ /index.php?\$query_string;
	}

	location ~ \\.php$ {
	include snippets/fastcgi-php.conf;
	fastcgi_pass unix:/run/php/$phpvers-fpm.sock;
	}

	location ~ /\\.ht {
	deny all;
	}
	}' > /etc/nginx/sites-available/$projName.local.conf"
	then
		echo "There is an ERROR create $projName file"
		exit;
	else
		echo "New Virtual Host Created"
	fi

	### Add domain in /etc/hosts
	if ! sudo sh -c "echo '127.0.0.1        $projName.local' >> /etc/hosts"
	then
		echo "ERROR: Not able write in /etc/hosts"
		exit;
	else
		printf "Host added to /etc/hosts file \\n"
	fi

	### enable website
	sudo ln -s /etc/nginx/sites-available/"$projName".local.conf /etc/nginx/sites-enabled/"$projName".local.conf

	### restart Nginx
	sudo service nginx restart
}

database_exists() {
    # shellcheck disable=SC2039
    local db_name
    db_name="$1"
    mysql --user="$2" --password="$3" --execute='show databases;' | grep -w "$db_name" >/dev/null
}

test_ssh_connection() {
    # shellcheck disable=SC2039
    local user
    # shellcheck disable=SC2039
    local host
    user="$1"
    host="$2"
		if [ "$DEBUG" = 'true' ]; then
			su --login --command "ssh -o StrictHostKeyChecking=no $user@$host :" "$(logname)"
		else
			su --login --command "ssh -o StrictHostKeyChecking=no $user@$host : 2>/dev/null" "$(logname)"
		fi
}

retrieve_database() {
	if [ "$#" != '3' ]; then
		echo "Wrong arg numbers takes user pass and project_name"
		exit 1
	else
		# shellcheck disable=SC2039
		local user=$1
		# shellcheck disable=SC2039
		local pass=$2
		# shellcheck disable=SC2039
		local projName=$3
		if ! database_exists "$projName" "$user" "$pass"; then
			echo "--------------  Creating database ---------------"
			mysql -u "$user" -p"$pass" -e "create database \`$projName\`"
		fi
		echo "--------------  Database created  ---------------"
	fi

	echo "-----------  Retrieving the database  -----------"
	if test_ssh_connection 'yeswedev' "dev.yeswedev.fr"
  then
    ssh_address="yeswedev@dev.yeswedev.fr"
  else
		read -rp "Please specify the ssh address from where you want to retrieve the database [example=user@server.com] : " ssh_address
	fi
	if [ "$DEBUG" = 'true' ]; then
		echo "copying most recent backup from ssh"
	fi
	most_recent="$(su --login --command "ssh -o StrictHostKeyChecking=no $ssh_address 'test -d yeswedev/backups/$projName/database && ls -t yeswedev/backups/$projName/database | head -1'" "$(logname)")"
	if [ "$most_recent" ]; then
		# shellcheck disable=SC2154
		su --login --command "scp $ssh_address:yeswedev/backups/$projName/database/$most_recent $userDir" "$(logname)"
		if [ "$DEBUG" = 'true' ]; then
			echo "copy done"
		fi
		if [ "$DEBUG" = 'true' ]; then
			echo "decompressing the backup"
		fi
		if [ "${most_recent##*.}" = 'gz' ]; then
			gunzip "$userDir"/"$most_recent"
			most_recent=${most_recent%.gz}
		fi
		if [ "$DEBUG" = 'true' ]; then
			echo "done"
		fi
		if [ "$DEBUG" = 'true' ]; then
			echo "importing the backup"
		fi
		mysql -u "$user" -p"$pass" "$projName" < "$most_recent"
		if [ "$DEBUG" = 'true' ]; then
			echo "import done"
		fi
		if [ "$DEBUG" = 'true' ]; then
			echo "deleting the backup from local storage"
		fi
		rm "$userDir"/"$most_recent"
		if [ "$DEBUG" = 'true' ]; then
			echo "deleted"
		fi
		echo "Database import completed"
	else
		echo "An error occured in importing the database"
	fi
}

retrieve_wordpress_upload() {
  check_dependencies 'rsync'

  if [ "$#" != '1' ]; then
		echo "Wrong arg numbers takes project_name"
		exit 1
	else
		# shellcheck disable=SC2039
		local projName=$1
	fi

  echo "-----------  Retrieving the upload files  -----------"
	if test_ssh_connection 'yeswedev' "dev.yeswedev.fr"
  then
    ssh_address="yeswedev@dev.yeswedev.fr"
  else
		read -rp "Please specify the ssh address from where you want to retrieve the uploads [example=user@server.com] : " ssh_address
	fi
	if [ "$DEBUG" = 'true' ]; then
		echo "copying most uploads from ssh"
	fi
	if [ -f "$userDir/$projName.git/deploy.php" ]; then
	  if [ "$DEBUG" = 'true' ]; then
	    echo "Deployer detected, looking for 'current' folder"
	  fi
		su --login --command "rsync -razpv $ssh_address:$projName.git/current/web/app/uploads/202* $PROJECT_PATH/web/app/uploads/" "$(logname)"
	else
	  if [ "$DEBUG" = 'true' ]; then
	    echo "$userDir/$projName.git/deploy.php not detected"
	  fi
	  su --login --command "rsync -razpv $ssh_address:$projName.git/web/app/uploads/202* $PROJECT_PATH/web/app/uploads/" "$(logname)"
	fi

}

update_env_file() {
	if [ "$#" != '3' ]; then
		echo "Wrong arg numbers takes user pass and project_name"
		exit 1
	fi

	declare -A  tab
	# Laravel
	tab[USERNAME]=$1
	tab[PASSWORD]=$2
	tab[DATABASE]=$3
	# Wordpress
	tab[USER]=$1
	tab[PASSWORD]=$2
	tab[NAME]=$3

	check_files "$PROJECT_PATH/.env"

	if [ "$DEBUG" = 'true' ]; then
		pwd
		echo ".env exists"
		printf "updating .env file\\r"
		sleep 0.5
		printf "updating .env file.\\r"
		sleep 0.5
		printf "updating .env file..\\r"
		sleep 0.5
		printf "updating .env file...\\r\\n"
		sleep 0.5 &
	fi

	# Laravel
	sed -i  "s/^\\(APP_NAME\\)=.*/\\1=$3/" "$PROJECT_PATH/.env"
	sed -i  "s/^\\(APP_URL\\)=.*/\\1=http:\\/\\/$PROJECT_HOSTNAME/" "$PROJECT_PATH/.env"
	for A in "DATABASE" "NAME" "USERNAME" "USER" "PASSWORD"; do
		sed -i  "s/^\\(DB_$A\\)=.*/\\1=${tab[$A]}/" "$PROJECT_PATH/.env"
	done
	# Wordpress
	sed -i  "s;^\\(WP_HOME\\)=https\\(.*\\);\\1=http\\2;" "$PROJECT_PATH/.env"

	if [ "$DEBUG" = 'true' ]; then
		echo ".env correctly updated"
	fi
}
